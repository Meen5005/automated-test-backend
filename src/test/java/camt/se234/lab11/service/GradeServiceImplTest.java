package camt.se234.lab11.service;

import junitparams.JUnitParamsRunner;
import junitparams.Parameters;
import junitparams.naming.TestCaseName;

import org.junit.Test;
import org.junit.runner.RunWith;


import static org.hamcrest.MatcherAssert.assertThat;
import static org.hamcrest.Matchers.is;
@RunWith(JUnitParamsRunner.class)
public class GradeServiceImplTest {
    @Test
    @Parameters(method = "paramsForTestGetGradeParams")
    @TestCaseName("Test getGrade Params [{index}] : input is {0}, expect \"{1}\"") //lab10 question the output of test case getGrade Params[0] input is 100 expect A
    public void testGetGradeparams(double score,String expectedGrade){
        GradeServiceImpl gradeService = new GradeServiceImpl();
        assertThat(gradeService.getGrade(score),is(expectedGrade));
    }
    public void testGetGradeparams(double midtermScore,double finalScore, String expectedGrade){
        GradeServiceImpl gradeService = new GradeServiceImpl();
        assertThat(gradeService.getGrade(midtermScore,finalScore),is(expectedGrade));
    }
    public void testGetGrade(){
        GradeServiceImpl gradeService = new GradeServiceImpl();
        assertThat(gradeService.getGrade(50,50),is("A"));
        assertThat(gradeService.getGrade(50,30),is("A"));
        assertThat(gradeService.getGrade(40,38.9),is("B"));
        assertThat(gradeService.getGrade(40,35),is("B"));
        assertThat(gradeService.getGrade(40,34.5),is("C"));
        assertThat(gradeService.getGrade(30,30),is("C"));
        assertThat(gradeService.getGrade(20,39.5),is("D"));
        assertThat(gradeService.getGrade(18,15),is("D"));
        assertThat(gradeService.getGrade(13.3,18.7),is("F"));
        assertThat(gradeService.getGrade(0,0),is("F"));
    }

    public Object paramsForTestGetGradeParams (){ //return 50%
        return new Object[][] {
                {100,"A"},
                {80,"A"},
                {78,"B"},
                {75,"B"},
                {74,"C"},
                {60,"C"},
                {59,"D"},
                {33,"D"},
                {32,"F"},
                {0,"F"}
        };

    }

   


}
