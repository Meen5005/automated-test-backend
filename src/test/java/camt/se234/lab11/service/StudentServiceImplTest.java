package camt.se234.lab11.service;


import camt.se234.lab11.dao.StudentDao;
import camt.se234.lab11.dao.StudentDaoImpl;
import camt.se234.lab11.entity.Student;
import org.junit.Before;
import org.junit.Test;


import java.util.ArrayList;
import java.util.List;

import static org.hamcrest.Matchers.hasItems;
import static org.hamcrest.Matchers.is;
import static org.hamcrest.core.IsNull.nullValue;
import static org.junit.Assert.assertThat;
import static org.mockito.Mockito.mock;
import static org.mockito.Mockito.when;

public class StudentServiceImplTest {
    StudentDao studentDao;  //8.2
    StudentServiceImpl studentService;
    @Test
    public void testFindById(){
        StudentDaoImpl studentDao = new StudentDaoImpl();
        StudentServiceImpl studentService = new StudentServiceImpl();
        studentService.setStudentDao(studentDao);
        assertThat(studentService.findStudentById("123"),is(new Student("123","A","temp",4.00)));
        assertThat(studentService.findStudentById("132"),is(new Student("132","B","temp",3.50)));
        assertThat(studentService.findStudentById("231"),is(new Student("231","C","temp",3.00)));
        assertThat(studentService.findStudentById("213"),is(new Student("213","D","temp",2.50)));
        assertThat(studentService.findStudentById("321"),is(new Student("321","A","temp",3.75)));
        assertThat(studentService.findStudentById("312"),is(new Student("312","B","temp",3.25)));
    }
    @Test
    public void getAverageGpa(){
        StudentDaoImpl studentDao = new StudentDaoImpl();
        StudentServiceImpl studentService = new StudentServiceImpl();
        studentService.setStudentDao(studentDao);
        assertThat(studentService.getAverageGpa(),is(3.3333333333333335));
    }

    //5.5 crate new test avg of new student
    @Test
    public void getAverageGpaNew(){
        StudentDaoImpl studentDao = new StudentDaoImpl();
        StudentServiceImpl studentService = new StudentServiceImpl();
        studentService.setStudentDao(studentDao);
        assertThat(studentService.getAverageGpaNew(),is(2.6666666666666665));
    }


    //6.2 mockito test findStudentById
    @Test
    public void testWithMock(){
        StudentDao studentDao = mock(StudentDao.class);
        List<Student> mockStudents= new ArrayList<>();
        StudentServiceImpl studentService = new StudentServiceImpl();
        studentService.setStudentDao(studentDao);
        mockStudents.add(new Student("123","A","temp",2.33));
        mockStudents.add(new Student("124","B","temp",2.33));
        mockStudents.add(new Student("223","C","temp",2.33));
        mockStudents.add(new Student("224","D","temp",2.33));
        when(studentDao.findAll()).thenReturn(mockStudents);
        assertThat(studentService.findStudentById("123"),is(new Student("123","A","temp",2.33)));
    }

    @Test
    public void testWithMockNew(){
       // StudentDao studentDao = mock(StudentDao.class);
        List<Student> mockStudents= new ArrayList<>();
       // StudentServiceImpl studentService = new StudentServiceImpl();
       // studentService.setStudentDao(studentDao);
        mockStudents.add(new Student("123","A","temp",4.00));
        mockStudents.add(new Student("132","B","temp",3.50));
        mockStudents.add(new Student("231","C","temp",3.00));
        mockStudents.add(new Student("213","D","temp",2.50));
        mockStudents.add(new Student("321","A","temp",3.75));
        mockStudents.add(new Student("312","B","temp",3.25));
        when(studentDao.findAll()).thenReturn(mockStudents);
        assertThat(studentService.findStudentById("213"),is(new Student("213","D","temp",2.50)));
        /*assertThat(studentService.findStudentById("132"),is(new Student("132","B","temp",3.50)));
        assertThat(studentService.findStudentById("231"),is(new Student("231","C","temp",3.00)));
        assertThat(studentService.findStudentById("213"),is(new Student("213","D","temp",2.50)));
        assertThat(studentService.findStudentById("321"),is(new Student("321","A","temp",3.75)));
        assertThat(studentService.findStudentById("312"),is(new Student("312","B","temp",3.25)));*/
    }

    @Test
    public void testDetAverageGpaMock1(){
        StudentDao studentDao = mock(StudentDao.class);
        List<Student> mockStudents= new ArrayList<>();
        StudentServiceImpl studentService = new StudentServiceImpl();
        studentService.setStudentDao(studentDao);
        mockStudents.add(new Student("123","A","temp",4.00));
        mockStudents.add(new Student("132","B","temp",3.50));
        mockStudents.add(new Student("231","C","temp",3.00));
        mockStudents.add(new Student("213","D","temp",2.50));
        when(studentDao.findAll()).thenReturn(mockStudents);
        assertThat(studentService.getAverageGpa(),is(3.25));

    }
    @Test
    public void testDetAverageGpaMock2(){
        StudentDao studentDao = mock(StudentDao.class);
        List<Student> mockStudents= new ArrayList<>();
        StudentServiceImpl studentService = new StudentServiceImpl();
        studentService.setStudentDao(studentDao);
        mockStudents.add(new Student("231","C","temp",3.00));
        mockStudents.add(new Student("213","D","temp",2.50));
        mockStudents.add(new Student("321","A","temp",3.75));
        mockStudents.add(new Student("312","B","temp",3.25));
        when(studentDao.findAll()).thenReturn(mockStudents);
        assertThat(studentService.getAverageGpa(),is(3.125));

    }
    @Test
    public void testDetAverageGpaMock3(){
        StudentDao studentDao = mock(StudentDao.class);
        List<Student> mockStudents= new ArrayList<>();
        StudentServiceImpl studentService = new StudentServiceImpl();
        studentService.setStudentDao(studentDao);
        mockStudents.add(new Student("132","B","temp",3.50));
        mockStudents.add(new Student("231","C","temp",3.00));
        mockStudents.add(new Student("213","D","temp",2.50));
        mockStudents.add(new Student("321","A","temp",3.75));
        when(studentDao.findAll()).thenReturn(mockStudents);
        assertThat(studentService.getAverageGpa(),is(3.1875));

    }


    //7.1 testFindByPartOfId
    @Test
    public void  testFindByPartOfId(){
        StudentDao studentDao = mock(StudentDao.class);
        List<Student> mockStudents = new ArrayList<>();
        StudentServiceImpl studentService = new StudentServiceImpl();
        studentService.setStudentDao(studentDao);
        mockStudents.add(new Student("123","A","temp",4.00));
        mockStudents.add(new Student("132","B","temp",3.50));
        mockStudents.add(new Student("231","C","temp",3.00));
        mockStudents.add(new Student("213","D","temp",2.50));
        mockStudents.add(new Student("321","A","temp",3.75));
        mockStudents.add(new Student("312","B","temp",3.25));
        when(studentDao.findAll()).thenReturn(mockStudents);
        assertThat(studentService.findStudentByPartOfId("12"),hasItems(new Student("123","A","temp",4.00)));
        assertThat(studentService.findStudentByPartOfId("12"),hasItems(new Student("123","A","temp",4.00),new Student("312","B","temp",3.25)));
    }
    @Test
    public void  testFindByPartOfId2(){
        StudentDao studentDao = mock(StudentDao.class);
        List<Student> mockStudents = new ArrayList<>();
        StudentServiceImpl studentService = new StudentServiceImpl();
        studentService.setStudentDao(studentDao);
        mockStudents.add(new Student("123","A","temp",4.00));
        mockStudents.add(new Student("132","B","temp",3.50));
        mockStudents.add(new Student("231","C","temp",3.00));
        mockStudents.add(new Student("213","D","temp",2.50));
        mockStudents.add(new Student("321","A","temp",3.75));
        mockStudents.add(new Student("312","B","temp",3.25));
        when(studentDao.findAll()).thenReturn(mockStudents);
        assertThat(studentService.findStudentByPartOfId("23"),hasItems(new Student("231","C","temp",3.00)));
        assertThat(studentService.findStudentByPartOfId("23"),hasItems(new Student("231","C","temp",3.00),new Student("123","A","temp",4.00)));
    }

    @Test
    public void  testFindByPartOfId3(){
        StudentDao studentDao = mock(StudentDao.class);
        List<Student> mockStudents = new ArrayList<>();
        StudentServiceImpl studentService = new StudentServiceImpl();
        studentService.setStudentDao(studentDao);
        mockStudents.add(new Student("123","A","temp",4.00));
        mockStudents.add(new Student("132","B","temp",3.50));
        mockStudents.add(new Student("231","C","temp",3.00));
        mockStudents.add(new Student("213","D","temp",2.50));
        mockStudents.add(new Student("321","A","temp",3.75));
        mockStudents.add(new Student("312","B","temp",3.25));
        when(studentDao.findAll()).thenReturn(mockStudents);
        assertThat(studentService.findStudentByPartOfId("13"),hasItems(new Student("132","B","temp",3.50)));
        assertThat(studentService.findStudentByPartOfId("13"),hasItems(new Student("132","B","temp",3.50),new Student("213","D","temp",2.50)));
    }


    //8.3 create method before test
    @Before
    public void setup(){
        studentDao = mock(StudentDao.class);
        studentService = new StudentServiceImpl();
        studentService.setStudentDao(studentDao);
    }

    //9.3 Add the test to test the exception
    @Test(expected = NoDataException.class)
    public void testNoDataException(){
        List<Student> mockStudents = new ArrayList<>();
        mockStudents.add(new Student("123","A","temp",4.00));
        mockStudents.add(new Student("132","B","temp",3.50));
        mockStudents.add(new Student("231","C","temp",3.00));
        mockStudents.add(new Student("213","D","temp",2.50));
        mockStudents.add(new Student("321","A","temp",3.75));
        mockStudents.add(new Student("312","B","temp",3.25));

        when(studentDao.findAll()).thenReturn(mockStudents);
        assertThat(studentService.findStudentById("55"),nullValue());
    }

    //9.4 Add the exception
    @Test(expected = NoDataException.class)
    public void testNoDataException2(){
        List<Student> mockStudents = new ArrayList<>();
        mockStudents.add(new Student("123","A","temp",4.00));
        mockStudents.add(new Student("132","B","temp",3.50));
        mockStudents.add(new Student("231","C","temp",3.00));
        mockStudents.add(new Student("213","D","temp",2.50));
        mockStudents.add(new Student("321","A","temp",3.75));
        mockStudents.add(new Student("312","B","temp",3.25));
        when(studentDao.findAll()).thenReturn(mockStudents);
        assertThat(studentService.findStudentByPartOfId("55"),nullValue());
    }

    @Test(expected = ArithmeticException.class)
    public void testGetAverageGpaException1() {
        List<Student> mockStudents = new ArrayList<>();
        when(studentDao.findAll()).thenReturn(mockStudents);
        assertThat(studentService.getAverageGpa(),nullValue());
    }

    @Test(expected = ArithmeticException.class)
    public void testGetAverageGpaException2() {
        List<Student> mockStudents = new ArrayList<>();
        when(studentDao.findAll()).thenReturn(mockStudents);
        assertThat(studentService.getAverageGpaNew(),nullValue());
    }

}
