package camt.se234.lab11.dao;

import camt.se234.lab11.entity.Student;

import java.util.ArrayList;
import java.util.List;

public class StudentDaoImpl implements StudentDao {

    List<Student> students;
    List<Student> newStudents;
    public StudentDaoImpl(){
        students = new ArrayList<>();
        students.add(new Student("123","A","temp",4.00));
        students.add(new Student("132","B","temp",3.50));
        students.add(new Student("231","C","temp",3.00));
        students.add(new Student("213","D","temp",2.50));
        students.add(new Student("321","A","temp",3.75));
        students.add(new Student("312","B","temp",3.25));

        //5.5 crate new student
        newStudents = new ArrayList<>();
        newStudents.add(new Student("555","B","temp",3.00));
        newStudents.add(new Student("505","B","temp",2.50));
        newStudents.add(new Student("005","C","temp",2.00));
        newStudents.add(new Student("500","D","temp",1.50));
        newStudents.add(new Student("550","A","temp",3.75));
        newStudents.add(new Student("055","B","temp",3.25));

    }

    @Override
    public List<Student> findAll() {
        return this.students;
    }

    //5.5 crate avg of new student
    @Override
    public  List<Student> findAllNew(){
        return this.newStudents;
    }

}
