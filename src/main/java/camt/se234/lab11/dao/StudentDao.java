package camt.se234.lab11.dao;

import camt.se234.lab11.entity.Student;

import java.util.List;

public interface StudentDao {
    List<Student> findAll();
    List<Student> findAllNew();     //5.5 crate avg of new student

}
